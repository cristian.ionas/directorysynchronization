﻿using Confluent.Kafka;
using OpenLm.DirectorySyncModels.Enums;
using System;
using System.Threading;

namespace DirectorySynchronization.ExampleProject
{
    class Program
    {
        static void Main(string[] args)
        {
            #region constants
            //To have all commented credentials below please contact OpenLM Support team
            //Insert the IPs of brokers needs to be "0.0.0.0:9092,0.0.0.1:9092"
            const string brokerList = "Enter the avaible IPs of brokers";

            //Insert your subscription topic name
            const string topicName = "Topic Name";
            #endregion

            var config = new ConsumerConfig
            {
                BootstrapServers = brokerList,
                SecurityProtocol = SecurityProtocol.SaslPlaintext,
                SaslMechanism = SaslMechanism.ScramSha256,
                //Insert your username
                SaslUsername = "username",
                //Insert your password
                SaslPassword = "passwordOLM",
                //Insert you group type
                GroupId = "external",
                EnableAutoCommit = true,
            };

            Console.WriteLine("The consumer has  started...");

            using var consumer = new ConsumerBuilder<Ignore, byte[]>(config).Build();
            consumer.Subscribe(topicName);
            try
            {
                while (true)
                {
                    try
                    {
                        var consumeResult = consumer.Consume(new CancellationToken(false));

                        if (consumeResult.IsPartitionEOF)
                        {
                            Console.WriteLine(
                                $"Reached end of topic {consumeResult.Topic}, partition {consumeResult.Partition}, offset {consumeResult.Offset}.");
                            continue;
                        }

                        Console.WriteLine($"\nReceived a message on topic {consumeResult.Topic}");

                        ProcessRequest(consumeResult.Message.Value, (ExternalRequestType)consumeResult.Partition.Value);

                    }
                    catch (ConsumeException e)
                    {
                        Console.WriteLine($"Consume error: {e.Error.Reason}");
                    }
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            consumer.Close();
        }

        /// <summary>
        /// Process request as ExternalRequestType
        /// </summary>
        /// <param name="byteArray">Byte Array</param>
        /// <param name="request">ExternalRequestType Enum</param>
        static void ProcessRequest(byte[] byteArray, ExternalRequestType request)
        {
            var requestHandler = new RequestHandler();
            switch (request)
            {
                case ExternalRequestType.StructureResultRequest:
                    requestHandler.HandleStructureResultRequest(byteArray);
                    break;
                case ExternalRequestType.NewEntitiesRequest:
                    requestHandler.HandleNewEntitiesRequest(byteArray);
                    break;
                case ExternalRequestType.ChangedEntitiesRequest:
                    requestHandler.HandleChangedEntitiesRequest(byteArray);
                    break;
                case ExternalRequestType.ChangedSyncEntitiesRequest:
                    requestHandler.HandleChangedSyncEntitiesRequest(byteArray);
                    break;
                case ExternalRequestType.DeletedSyncEntitiesRequest:
                    requestHandler.HandleDeletedSyncEntitiesRequest(byteArray);
                    break;
            }
        }
    }
}
