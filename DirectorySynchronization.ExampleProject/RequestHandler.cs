﻿using OpenLm.DirectorySyncModels.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;

namespace DirectorySynchronization.ExampleProject
{
    public class RequestHandler
    {

        /// <summary>
        /// Handle new entities request
        /// </summary>
        /// <param name="byteArray">Byte Array</param>
        public void HandleStructureResultRequest(byte[] byteArray)
        {
            var structureResult = JsonSerializer.Deserialize<DsStructureResultRequest>(byteArray);

            Console.WriteLine($"\nStructureResult from jobId {structureResult.JobId} with {structureResult.SyncStructureResult.NewEntities.Count} newEntities, {structureResult.SyncStructureResult.ChangedEntities.Count} changedEntities, " +
                              $"{structureResult.SyncStructureResult.ChangedSyncEntities.Count} changedSyncEntities and {structureResult.SyncStructureResult.DeletedSyncEntities.Count} deletedSyncEntities received");
        }

        /// <summary>
        /// Handle new entities request
        /// </summary>
        /// <param name="byteArray">Byte Array</param>
        public void HandleNewEntitiesRequest(byte[] byteArray)
        {
            var entities = JsonSerializer.Deserialize<IList<DsEntity>>(byteArray);

            Console.WriteLine($"\n{entities.Count} new entities received");

            int n = 0;
            foreach (var entity in entities)
            {
                n++;
                Console.WriteLine($"\n[{n}] Name: {entity.Name}, EntityType: {entity.EntityType}, LdapUniqueId: {entity.LdapUniqueId}, LastSyncDate: {entity.LastSyncDate}, Properties: [{(entity.Properties != null ? string.Join("; ", entity.Properties.Select(x => x.Key + "=" + x.Value).ToArray()) : default)}]");
            }
        }

        /// <summary>
        /// Handle changed entities request
        /// </summary>
        /// <param name="byteArray">Byte Array</param>
        public void HandleChangedEntitiesRequest(byte[] byteArray)
        {
            var entities = JsonSerializer.Deserialize<IList<DsEntity>>(byteArray);

            Console.WriteLine($"\n{entities.Count} changed entities received");

            int n = 0;
            foreach (var entity in entities)
            {
                n++;
                Console.WriteLine($"\n[{n}] Name: {entity.Name}, EntityType: {entity.EntityType}, LdapUniqueId: {entity.LdapUniqueId}, LastSyncDate: {entity.LastSyncDate}, Properties: [{(entity.Properties != null ? string.Join("; ", entity.Properties.Select(x => x.Key + "=" + x.Value).ToArray()) : default)}]");
            }
        }

        /// <summary>
        /// Handle Changed sync entities request
        /// </summary>
        /// <param name="byteArray">Byte Array</param>
        public void HandleChangedSyncEntitiesRequest(byte[] byteArray)
        {
            var syncEntities = JsonSerializer.Deserialize<IList<DsSyncEntity>>(byteArray);

            Console.WriteLine($"\n{syncEntities.Count} changed sync entities received");

            int n = 0;
            foreach (var syncEntity in syncEntities)
            {
                n++;
                Console.WriteLine($"\n[{n}] Path: {syncEntity.Path}, EntityRelation: {syncEntity.Entity.LdapUniqueId}, Parent: {syncEntity.Parent?.Path}, LastSyncDate: {syncEntity.LastSyncDate}, ShouldBeSync: {syncEntity.ShouldBeSync}");
            }
        }

        /// <summary>
        /// Handle Deleted sync entities request
        /// </summary>
        /// <param name="byteArray">Byte Array</param>
        public void HandleDeletedSyncEntitiesRequest(byte[] byteArray)
        {
            var syncEntities = JsonSerializer.Deserialize<IList<DsSyncEntity>>(byteArray);

            Console.WriteLine($"\n{syncEntities.Count} deleted sync entities received");

            int n = 0;

            foreach (var syncEntity in syncEntities)
            {
                n++;
                Console.WriteLine($"\n[{n}] Path: {syncEntity.Path}, EntityRelation: {syncEntity.Entity.LdapUniqueId}, Parent: {syncEntity.Parent?.Path}, LastSyncDate: {syncEntity.LastSyncDate}, ShouldBeSync: {syncEntity.ShouldBeSync}");
            }
        }
    }
}
