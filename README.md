# DirectorySynchronization.ExampleProject
This project is an example for external vendors for consuming the data to their applications.

# Settings steps
Consolidated repo from:
- Create an account in [OpenLM Cloud](https://eu-cloud.openlm.com) or submit a request on [LdapSynchronization.com](https://ldapsynchronization.com)
- Ask for support team to offer access to Directory Synchronization in cloud, and specify you need option to consume data externaly
- Download DSA, install and approve it in your cloud account of DSS,
- With credentials offered by support change in Program.cs: the brokerList, topicName, SaslUsername, SaslPassword and GroupId parameters,
- Start the application, if you have some messages for consumption, the console will display it
